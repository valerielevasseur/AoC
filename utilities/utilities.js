import fs from "fs";

/**
 * Returns content of file as string
 * @param url to get file
 * @returns {string} file content or ''
 */
function getFile(url){
    try {
        return fs.readFileSync(url, 'utf8');
    } catch (err) {
        return '';
    }
}

/**
 * Returns content of file as string
 * @param url to get file
 * @param blocks if the file is divided in blocks divided by blank line (ex. coordinates and instructions)
 * @param parseNumber if each line is a number
 * @returns {*[]} file content or ''
 */
function getLinesFromFile(url = '', blocks=false, parseNumber = false){
    const inputStr = getFile(url);
    let input = [];
    if(blocks){
        input = inputStr.split(/\r\n\r\n/g).map(
            bloc => parseNumber ? bloc.split(/\r\n/).map(line => parseInt(line,10)): bloc.split(/\r\n/))
    }
    else{
        input = inputStr.split(/\r\n/).map(line => parseNumber ? line => parseInt(line,10): line);
    }
    return input;
}

/**
 * Sort a collection of objects by property or function
 * @param items collection of objects
 * @param prop property name (string)
 * @param isfx boolean is the property a function
 * @param asc sort order true if asc, false if desc
 * @returns {*} the collection (now sorted)
 */

function sortByProp(items= [], prop, isfx = false, asc=true){
    if(isfx){
        items.sort((a, b) => asc ? a[prop]() - b[prop]() : b[prop]() - a[prop]());
    }
    else{
        items.sort((a, b) => asc ? a[prop] - b[prop] : b[prop] - a[prop]);
    }
    return items;
}

/**
 * Returns sum of a property in the collection
 * @param items collection of items
 * @param prop property name (string)
 * @param ifFx boolean is the property a function
 * @returns {*} sum of property
 */
function getSumOfProp(items = [], prop, ifFx=false){
    return items.reduce((sum, item) => ifFx ? sum + item[prop]() : sum + item[prop],0);
}

/**
 * Create windows of values
 * @param input
 * @param windowSize
 * @returns {*[]}
 */
function createWindows(input, windowSize=4) {
    let windows = [];
    input.split('').forEach((item, index) => {
        if (index > (windowSize-2) && index < input.length) {
            let window = [];
            for(let i=windowSize-1; i>=0; i--){
                window.push(input[index - i]);
            }
            windows.push(window.join(''));
        }
    })
    return windows;
}

export {getFile, sortByProp, getSumOfProp, getLinesFromFile, createWindows}
