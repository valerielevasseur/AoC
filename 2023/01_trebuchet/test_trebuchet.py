import pytest
import trebuchet


@pytest.mark.parametrize("ligne, expected", [
    ('two1nine', 29),
    ('lfbhcszfjhdcmdnfgkjmlzggcxvqxmsznqhhmfourrqkhdzzqnn9twonezlp', 41)
])
def test_calibration_p2(ligne, expected):
    assert trebuchet.calibration_p2(ligne) == expected
