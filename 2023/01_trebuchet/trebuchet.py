import regex as re


def calibration_p1(ligne: str) -> int:
    """
    Retourne le chiffre de calibration de la ligne, le nombre composé du premier et du dernier chiffre de la ligne
    :param ligne: ligne d'entrée ex. pqr3stu8vwx
    :return: le nombre entier composé du premier et du dernier chiffre
    """
    nombres = re.findall(r"\d", ligne)
    return int(nombres[0] + nombres[-1])


def calibration_p2(ligne: str) -> int:
    """
    Retourne le chiffre de calibration de la ligne, le nombre composé du premier et du dernier chiffre de la ligne
    :param ligne: ligne d'entrée ex. pqr3stu8vwx
    :return: le nombre entier composé du premier et du dernier chiffre
    """
    nombres = re.findall(r"\d|one|two|three|four|five|six|seven|eight|nine", ligne, overlapped=True)
    dict_nombres = {'1': '1', '2': '2', '3': '3', '4': '4', '5': '5', '6': '6', '7': '7', '8': '8', '9': '9',
                    'one': '1', 'two': '2', 'three': '3', 'four': '4',
                    'five': '5', 'six': '6', 'seven': '7', 'eight': '8', 'nine': '9'}
    return int(dict_nombres[nombres[0]] + dict_nombres[nombres[-1]])


somme = 0
with open("input.txt", mode="r", encoding="utf-8") as fichier:
    contenu = fichier.read().splitlines()

for ligne in contenu:
    somme += calibration_p2(ligne)

print(somme)
