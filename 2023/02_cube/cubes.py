import re
import numpy as np


def is_possible(game: str) -> int:
    game_id = int(re.findall(r"Game (\d+):", game)[0])
    game_colors = {'red': 0, 'green': 0, 'blue': 0}
    turns = game.split(';')
    for turn in turns:
        groups = re.findall(r"((\d+) (red|green|blue))", turn)
        for group in groups:
            game_colors[group[2]] = int(group[1]) if int(group[1]) > game_colors[group[2]] else game_colors[group[2]]
    if (game_colors['red'] <= 12 and game_colors['green'] <= 13
            and game_colors['blue'] <= 14):
        return game_id
    else:
        return 0


def power(game: str) -> int:
    game_colors = {}
    turns = game.split(';')
    for turn in turns:
        groups = re.findall(r"((\d+) (red|green|blue))", turn)
        for group in groups:
            if not (group[2] in game_colors.keys()) or int(group[1]) > game_colors[group[2]]:
                game_colors[group[2]] = int(group[1])
    return np.prod(list(game_colors.values()))


somme = 0
with open("input.txt", mode="r", encoding="utf-8") as fichier:
    contenu = fichier.read().splitlines()

for ligne in contenu:
    somme += power(ligne)

print(somme)
