import {getLinesFromFile, getSumOfProp} from "../../utilities/utilities.js";

function Node(name, size = 0, isDir = false){
    this.name = name;
    this.contains = [];
    this.parent = null;
    this.isDir = isDir;
    this.size = size;
}

const root = new Node('/', 0, true);
let pathList = [root];
let currentDir = root;

function treatInstruction(line){
    if(line.includes('$ cd ..')){
        currentDir.parent.size += currentDir.size;
        currentDir = currentDir.parent;
    }
    else if(line.includes('$ cd /')){
        currentDir = root;
    }
    else if(line.includes('$ cd')){
        const content = line.split(' ');
        currentDir = currentDir.contains.find(el => el.name ===  content[2]);
    }
    else if(line === ''){
        let sum = 0;
        for(let i=0; i<root.contains.length; i++){
            sum +=root.contains[i].size;
        }
        root.size = sum;
    }
    else if(!/^\$ ls/.test(line)){
       const content = line.split(' ');
       if(content[0] === 'dir'){
           let dir = new Node(content[1], 0, true)
           dir.parent = currentDir;
           pathList.push(dir);
           currentDir.contains.push(dir);
       }
       else{
           const file = new Node(content[1], parseInt(content[0]), false);
           file.parent = currentDir;
           currentDir.contains.push(file);
           currentDir.size += parseInt(content[0]);
           pathList.push(file);
       }
    }
}

function part1(){
    let sum = 0;
    for(let i=1; i<pathList.length; i++){
        if (getSumOfProp(pathList[i].contains, 'size', false) <= 100000) sum += getSumOfProp(pathList[i].contains, 'size', false); //P1
    }
    return sum;
}

function getTarget(){
    return 30000000-(70000000 - root.size);
}

const list= getLinesFromFile('./inputs/jour7.txt')//
list.forEach(line => treatInstruction(line))

//P1
console.log(part1());

//P2
const candidates = pathList.filter(node => node.isDir && node.size >= getTarget()).map(item=>item.size)
candidates.sort((a, b) => a - b)
console.log(candidates[0])
