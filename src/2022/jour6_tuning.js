import {createWindows, getLinesFromFile} from "../../utilities/utilities.js";

function isDifferent(input){
    for(let i =0; i<input.length; i++){
        if(input.indexOf(input.charAt(i), i+1)>-1){
            return false;
        }
    }
    return true;
}


const list= getLinesFromFile('./inputs/jour6.txt')//.map(pair => new Pair(pair));
const windowSize = 14;
const windows = createWindows(list[0],windowSize);
windows.forEach((window, index) => {
    if(isDifferent(window)){
        console.log(windowSize+index);
    }
})
