import {getLinesFromFile, sortByProp} from "../../utilities/utilities.js";

function checkRight (tree, map) {
    let visible = true;
    let extVal = map[tree.x][map[tree.x].length-1].val;
    for(let y=map[tree.x].length-1; y>tree.y; y--){
        if((map[tree.x][y].val >= tree.val || map[tree.x][y].val > extVal)){
            visible = false;
            break;
        }
    }
    return visible;
}

function checkLeft (tree, map) {
    let visible = true;
    let extVal = map[tree.x][0].val;
    for(let item of map[tree.x]){
        if((item.y < tree.y && item.val >= tree.val) || (item.val > extVal && item.y > 0 && item.y < tree.y)){
            visible = false;
            break;
        }
    }
    return visible;
}

function checkUp (tree, map) {
    let visible = true;
    let extVal = map[0][tree.y].val;
    for(let line in map){
        if( (map[line][tree.y].val >= tree.val && line < tree.x) || (map[line][tree.y].val >= extVal && line < tree.x && line>0)){
            visible = false;
            break;
        }
    }
    return visible;
}

function checkDown (tree, map) { //4-Tree(3)-2-1
    let visible = true;
    let extVal = map[map.length-1][tree.y].val;
    for(let line in map){
        if((map[line][tree.y].val > tree.val && line > tree.x)||(map[line][tree.y].val >= extVal && line > tree.x && line<map.length-1)){
            visible = false;
            break;
        }
    }
    return visible;
}

export function Tree (x, y, val){
    this.x = x;
    this.y = y;
    this.val = val;
    this.visible = false;
    this.toString = () => `(${this.x},${this.y},${this.val},${this.visible})`;
    this.checkVisibility = function(map){
        if(this.x === 0 || this.y === 0 || this.x === map.length-1 || this.y === map[0].length-1){ //edge
            this.visible = true;
            return true;
        }
        else if(checkLeft(this,map)||checkRight(this,map)||checkUp(this,map)||checkDown(this, map)){
            this.visible = true;
            return true;
        }
        return this.visible = false;
    }
}

function getNbVisible(url){
    let lst = getLinesFromFile(url);
    lst.pop();
    lst = lst.map((line,iL) => line.split('').map((item, iI) => new Tree(iL, iI, parseInt(item, 10))));
    let nbvisibles = 0;
    for(let x in lst){
        for(let y in lst[x]){
            if(lst[x][y].checkVisibility(lst)) {
                nbvisibles++;
            }
            console.log(lst[x][y].toString())
        }
    }
    return nbvisibles;
}


//console.log(lst)
console.log(getNbVisible('./inputs/jour8.txt'));
export{getNbVisible, checkLeft, checkRight, checkUp, checkDown}
