import {getLinesFromFile, sortByProp} from "../../utilities/utilities.js";

function Elf(meals = []){
    this.meals = meals;
    this.total_meals = () => meals.reduce((sum, meal) => sum + parseInt(meal,10),0)
}

function sortDescElves(elves){
    sortByProp(elves, 'total_meals', true, false);
    return elves;
}

const elves = getLinesFromFile('./inputs/jour1.txt', true, true).map(meal => new Elf(meal));
sortDescElves(elves);

console.log(elves[0].total_meals()); // partie 1
console.log(elves[0].total_meals()+elves[1].total_meals()+elves[2].total_meals()); // partie 2
