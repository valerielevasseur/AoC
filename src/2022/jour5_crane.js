import {getFile} from "../../utilities/utilities.js";

function Stack(no){
    this.no = no;
    this.list = []
}

function Move(qty, from, to){
    this.qty = qty;
    this.from = from;
    this.to = to;
}

function makeMove(stackList = [], move){
    let origin = stackList.find(stack => stack.no === move.from);
    let packages = origin.list.splice(origin.list.length-move.qty, move.qty)//.reverse(); P2
    let dest = stackList.find(stack => stack.no === move.to);
    dest.list = dest.list.concat(packages);
}

function prepareStacks(input){
    const lstStack = [];
    const lstMoves = [];
    const ligneNo = input.findIndex(line => line === '')-1;
    input[ligneNo].trim().split(/\s{3}/).forEach((item, index) => {
        lstStack.push(new Stack(item))
    });
    for(let row=0; row < ligneNo; row++){
        for(let iS=0; iS< lstStack.length; iS++){
            lstStack[iS].list.push(input[row].substring(iS*4, iS*4+3).trim().replaceAll(/[\[\]]/g,''));
        }
    }
    lstStack.forEach(stack => {
        stack.list.reverse();
        stack.list = stack.list.join('').split('');
    });
    for(let i = ligneNo+2; i<input.length; i++){
        const move = input[i].substring(5).replaceAll(/\s+(from|to|move)\s+/g,'|').split(/\|/);
        lstMoves.push(new Move(parseInt(move[0]), move[1], move[2]))
    }
    return {lstStack, lstMoves};
}

function getTopStack(listStack){
    let response = '';
    listStack.forEach(stack => response+=stack.list[stack.list.length-1]);
    return response;
}

const input = getFile('./inputs/jour5.txt').split(/\r\n/);
const {lstStack, lstMoves} = prepareStacks(input);
lstMoves.forEach(move => makeMove(lstStack, move));
console.log(getTopStack(lstStack));


