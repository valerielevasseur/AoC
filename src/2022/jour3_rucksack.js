import {getLinesFromFile} from "../../utilities/utilities.js";

function Rucksack(fullrucksack){
    this.fullsack = fullrucksack;
    this.c1 = fullrucksack.slice(0, Math.ceil(fullrucksack.length/2))
    this.c2 = fullrucksack.slice(Math.ceil(fullrucksack.length/2))
    this.commonType = this.c1.split('').filter(x => this.c2.includes(x))[0];
    this.commonTypePriority = getPriority(this.commonType);
}

function getBadge(r1, r2, r3){
    return r1.split('').filter(x => r2.includes(x) && r3.includes(x))[0];
}

/**
 * A -> ascii 65; priority 27
 * a -> ascii 97; priority 1
 * @param char
 * @returns {number}
 */
function getPriority(char){
    const ascii = char.charCodeAt(0);
    return ascii > 96 ? ascii-96 : ascii-38;
}

function getSumPriorities(sacks = []){
    return sacks.reduce((sum, sack) => sum + sack.commonTypePriority,0)
}

// Partie 1
const lst = getLinesFromFile('./inputs/jour3.txt').map(sack => new Rucksack(sack));
console.log(getSumPriorities(lst));

// Partie 2
let sum = 0;
for(let i=1; i<=lst.length; i++){
    if(i%3 === 0){ //groupe
        sum += getPriority(getBadge(lst[i-3].fullsack, lst[i-2].fullsack, lst[i-1].fullsack))
    }
}
console.log(sum);
