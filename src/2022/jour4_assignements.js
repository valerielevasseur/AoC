import {getLinesFromFile} from "../../utilities/utilities.js";

function Assignement(assignementStr){
    this.start = parseInt(assignementStr.split('-')[0],10);
    this.end = parseInt(assignementStr.split('-')[1],10);
}

function Pair(pair){
    this.pair = pair;
    this.a1 = new Assignement(pair.split(',')[0]);
    this.a2 = new Assignement(pair.split(',')[1]);
    this.contains = () => {
        return (this.a1.end >= this.a2.end && this.a1.start <= this.a2.start) || (this.a2.end >= this.a1.end && this.a2.start <= this.a1.start );
    }
    this.overlap = () => {
        return this.contains() || (this.a1.end >= this.a2.start && this.a1.end <= this.a2.end) ||(this.a2.end >= this.a1.start && this.a2.end <= this.a1.end);
    }
}

function getNbFC(list){
   return list.reduce((sum, pair) => sum + pair.contains(),0)
}

function getNbOverlap(list){
    return list.reduce((sum, pair) => sum + pair.overlap(),0)
}


const listPairs = getLinesFromFile('./inputs/jour4.txt').map(pair => new Pair(pair));
console.log(getNbFC(listPairs)); //p1
console.log(getNbOverlap(listPairs)); //p2
