import {getFile, getSumOfProp} from "../../utilities/utilities.js";

function Shape(name, opponent, player, score){
    this.name = name;
    this.opponent = opponent;
    this.player = player;
    this.score = score;
}

const shapes = [
    new Shape('rock', 'A', 'X', 1),
    new Shape('paper', 'B', 'Y', 2),
    new Shape('scissors', 'C', 'Z', 3)
];

function Round(play, shapes){
    this.play = play;
    this.myShape = shapes.find(shape => shape.player ===  play.split(' ')[1]);
    this.outcome = () => {
        if(play.split('')[0] === this.myShape.opponent) return 3;
        switch (play) {
            case 'A Y' :
            case 'B Z' :
            case 'C X' :
                return 6; // won
            default:
                return 0; // lost
        }

    }
    this.totalScore = () => this.myShape.score + this.outcome();
}

function translateRound(instruction, shapes){
    //draw
    if(instruction.split(' ')[1] === 'Y'){
        return `${instruction.split(' ')[0]} ${shapes.find(shape => shape.opponent ===  instruction.split(' ')[0]).player}`
    }
    switch (instruction) {
        case 'A X' : // loose vs rock
        case 'B Z' : // win vs paper
            return `${instruction.split(' ')[0]} Z` // scissors
        case 'A Z' : // win vs rock
        case 'C X' : // loose vs scissors
            return `${instruction.split(' ')[0]} Y` // paper
        case 'B X' : // loose vs paper
        case 'C Z' : // win vs scissors
            return `${instruction.split(' ')[0]} X` // rock
    }
}

function Tournament(rounds = []){
    this.rounds = rounds;
    this.totalScore = () => getSumOfProp(this.rounds, 'totalScore', true);
}

/* Partie 1
const tournament = new Tournament();
const roundsStr = getFile('./inputs/jour2.txt').split(/\r\n/);
roundsStr.forEach(round => {
    tournament.rounds.push(new Round(round, shapes))
})

console.log(tournament.totalScore())
*/

// Partie 2
const tournament = new Tournament();
const roundsStr = getFile('./inputs/jour2.txt').split(/\r\n/);
roundsStr.forEach(round => {
    tournament.rounds.push(new Round(translateRound(round, shapes), shapes))
})
console.log(tournament.totalScore())
