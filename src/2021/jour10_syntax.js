import {getFile} from "../../utilities/utilities.js";

/*
 * If a chunk opens with (, it must close with ).
 * If a chunk opens with [, it must close with ].
 * If a chunk opens with {, it must close with }.
 * If a chunk opens with <, it must close with >.
 *
 * Tout ce qui est ouvert doit être refermé.
 * Si la pile est vide, tout est ok
 *
 */

function sumArray(input) {
    const array = input.slice();
    const reducer = (total, current) => total + current;
    return (array.reduce(reducer));
}

function part1(input){
    const open = ['(', '[', '{', '<'];
    const close = [')', ']', '}', '>'];
    const points = [3,57,1197, 25137]
    return sumArray(input.map((ligne) => {
        let nextClose = '';
        let pile = []
        for (let i = 0; i < ligne.length; i++) {
            let indexOpen = open.indexOf(ligne[i]);
            if (indexOpen > -1) {
                pile.push(ligne[i]);
                nextClose = close[indexOpen];
            }
            else {
                if (nextClose === ligne[i]) {
                    pile.pop();
                    indexOpen = open.indexOf(pile[pile.length - 1]);
                    nextClose = close[indexOpen]
                }else{
                    console.log('expected', nextClose, 'received', ligne[i]);
                    return points[close.indexOf(ligne[i])];
                }
            }
        }
        return 0;
    }));
}

function part2(input){
    const open = ['(', '[', '{', '<'];
    const close = [')', ']', '}', '>'];
    const points = [1,2,3,4];

    let res = input.map((ligne) => {
        let nextClose = '';
        let pile = []
        for (let i = 0; i < ligne.length; i++) {
            let indexOpen = open.indexOf(ligne[i]);
            if (indexOpen > -1) {
                pile.push(ligne[i]);
                nextClose = close[indexOpen];
            }
            else {
                if (nextClose === ligne[i]) {
                    pile.pop();
                    indexOpen = open.indexOf(pile[pile.length - 1]);
                    nextClose = close[indexOpen]
                }else{
                    return 0;
                }
            }
        }
        //incomplete
        if(pile.length > 0){
            let total = 0;
            pile.reverse();
            pile.forEach((char)=>{
                total = (5*total) + points[open.indexOf(char)];
            })
            return total;
        }
        return 0;
    });
    res = res.filter((el)=>el>0).sort(function(a,b){return a - b});
    return res[Math.round(res.length/2)-1];
}

const input = getFile('./inputs/jour10.txt').split(/\r\n/);
console.log(part1(input));
console.log(part2(input));
