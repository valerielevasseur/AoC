import {getFile} from "../../utilities/utilities.js";

function Point(x, y, energy) {
    this.x = x;
    this.y = y;
    this.energy = energy;
    this.step = () => {
        this.energy = (this.energy + 1) % 10;
        return this.energy;
    }
}

function getDir(map, direction, i, j) {
    switch (direction) {
        case 'u':
            return i > 0 ? new Point(i - 1, j, map[i - 1][j]) : null;
        case 'd':
            return i < map.length - 1 ? new Point(i + 1, j, map[i + 1][j]) : null;
        case 'l':
            return j > 0 ? new Point(i, j - 1, map[i][j - 1]) : null;
        case 'r':
            return j < map[i].length - 1 ? new Point(i, j + 1, map[i][j + 1]) : null;
        case 'ul':
            return (i > 0 && j > 0) ? new Point(i - 1, j - 1, map[i - 1][j - 1]) : null;
        case 'ur':
            return (i > 0 && j < map[i].length - 1) ? new Point(i - 1, j + 1, map[i - 1][j + 1]) : null;
        case 'dl':
            return (i < map.length - 1 && j > 0) ? new Point(i + 1, j - 1, map[i + 1][j - 1]) : null;
        case 'dr':
            return (i < map.length - 1 && j < map[i].length - 1) ? new Point(i + 1, j + 1, map[i + 1][j + 1]) : null;
    }
    return null;
}

function step(map) {
    let flashes = [];
    for (let i = 0; i < map.length; i++) {
        for (let j = 0; j < map[0].length; j++) {
            map[i][j] = (map[i][j] + 1) % 10;
            if (map[i][j] === 0) flashes.push(new Point(i, j, 0))
        }
    }
    const dir = ['u', 'd', 'l', 'r', 'ul', 'ur', 'dl', 'dr'];
    for (let octopus of flashes) {
        map[octopus.x][octopus.y] = 0;
        dir.forEach((direction) => {
            let candidate = getDir(map, direction, octopus.x, octopus.y);
            if (candidate) {
                const inlist = (point) => point.x === candidate.x && point.y === candidate.y;
                if (flashes.findIndex(inlist) === -1) {
                    map[candidate.x][candidate.y] = (map[candidate.x][candidate.y] + 1) % 10;
                    if (map[candidate.x][candidate.y] === 0) {
                        flashes.push(candidate)
                    }
                }
            }
        });
    }
    return flashes.length;
}

function getFlashes(steps, map) {
    let total = 0;
    for (let i = 1; i <= steps; i++) {
        total += step(map);
    }
    return total
}

function part2(steps, real){
    let i =0;
    while(true){
        if(step(real) === steps){
            break;
        }
        i++
    }
    return(i+1)
}


const lines = getFile('./inputs/jour11.txt').split(/\r\n/);
const real = [];
lines.forEach(line => {
    real.push(line.split('').map(item => parseInt(item,10)));
})


// console.log(getFlashes(100, real)) //p1
console.log(part2(100, real))
