import {getFile} from "../../utilities/utilities.js";

function calculateFuelRate(input, target){
    const reducer = (fuel, currentValue) => {
        let n = Math.abs(currentValue - target);
        return fuel + (n*(n+1))/2;
    };
    return(input.reduce(reducer, 0));
}

function calculateFuel(input, target){
    const reducer = (fuel, currentValue) => fuel + Math.abs(currentValue - target);
    return(input.reduce(reducer, 0));
}

function part2(input){
    input.sort();
    let number = -1;
    let calcfuel = 0;
    for (let candidate=1;candidate <input[input.length-1]; candidate++){
        calcfuel = calculateFuelRate(input, candidate);
        if(number < 0) number = calcfuel;
        number = calcfuel<number ? calcfuel : number;
    }
    return number
}
const input = getFile('./inputs/jour7.txt').split(',').map(item => parseInt(item,10));
console.log(part2(input));
