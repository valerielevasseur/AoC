import {getFile} from "../../utilities/utilities.js";

function getNbIncrease(input) {
    let max = 0;
    input.forEach((item, index) => {
        if (parseInt(item,10) > parseInt(input[index - 1],10)) {
            max++;
        }
    })
    return max;
}

function createWindows(input) {
    let windows = [];
    input.forEach((item, index) => {
        if (index > 0 && index < input.length - 1) {
            windows.push(parseInt(input[index - 1],10) + parseInt(item,10) + parseInt(input[index + 1],10));
        }
    })
    return windows;
}

function getNbIncreaseWindows(input){
    return getNbIncrease(createWindows(input));
}

const depth = getFile('./inputs/jour1.txt').split(/\n/);
console.log(getNbIncrease(depth));
console.log(getNbIncreaseWindows(depth));

export {getNbIncrease, getNbIncreaseWindows}
