import {getFile} from "../../utilities/utilities.js";

function Point(x, y, height) {
    this.x = x;
    this.y = y;
    this.height = height;
    this.risk = () => this.height + 1
}

function getDir(map, direction, i, j) {
    switch (direction) {
        case 'u':
            return i > 0 ? new Point(i-1, j,map[i - 1][j]) : new Point(i-1, j,9);
        case 'd':
            return i < map.length - 1 ? new Point(i+1, j,map[i + 1][j])  : new Point(i+1, j,9);
        case 'l':
            return j > 0 ? new Point(i, j-1,map[i][j - 1])  : new Point(i, j-1,9);
        case 'r':
            return j < map[i].length - 1 ? new Point(i, j+1,map[i][j+1]) : new Point(i, j+1,9);
    }
    return new Point(-1, -1,9);
}

function getLowPoints(map) {
    let heightmap = []
    let dir = [9, 9, 9, 9] //up, down, left, right
    for (let i = 0; i < map.length; i++) {
        for (let j = 0; j < map[0].length; j++) {
            dir = [
                getDir(map, 'u', i, j).height,
                getDir(map, 'd', i, j).height,
                getDir(map, 'l', i, j).height,
                getDir(map, 'r', i, j).height
            ]
            if (dir.filter(point => point > map[i][j]).length === dir.length) {
                heightmap.push(new Point(i, j, map[i][j]));
            }
        }
    }
    let total = 0;
    heightmap.forEach((point) => {
        total += point.risk();
    })
    return {heightmap, total};
}

function part2(map) {
    let low = getLowPoints(map).heightmap;
    let basins = low.map((lowPoint) => {
        let basin = [lowPoint];
        for(let lowPoint of basin){
            const dir = [
                getDir(map, 'u', lowPoint.x, lowPoint.y),
                getDir(map, 'd', lowPoint.x, lowPoint.y),
                getDir(map, 'l', lowPoint.x, lowPoint.y),
                getDir(map, 'r', lowPoint.x, lowPoint.y)
            ]
            dir.forEach((direction) => {
               if(direction.height > lowPoint.height && direction.height !== 9){
                   //Si n'est pas déjà dans le basin
                   const dansBasin = (point) => point.x === direction.x && point.y === direction.y;
                   if(basin.findIndex(dansBasin) === -1){
                       basin.push(direction)
                   }
               }
            });
        }
        return basin.length;
    })
    basins.sort(function(a,b){return a - b});
    const x = basins.length;
    return basins[x-1] * basins[x-2] * basins[x-3];
}

const lines = getFile('./inputs/jour9.txt').split(/\r\n/);
const map = [];
lines.forEach(line => {
    map.push(line.split('').map(item => parseInt(item,10)));
})
console.log(getLowPoints(map));
console.log(part2(map));
