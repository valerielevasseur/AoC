import {getFile} from "../../utilities/utilities.js";

function initCarte(m,n){
    let arr = new Array(m);
    for (let i = 0; i < m; i++) {
        arr[i] = new Array(n);
        for (let j = 0; j < m; j++) {
            arr[i][j] = 0;
        }
    }
    return arr;
}

function getNbIntersect(input, n){
    let carte = creerCarte(input, n);
    let count = 0;
    for (let i = 0; i < carte.length; i++) {
        for (let j = 0; j < carte[0].length; j++) {
            if(carte[i][j] >= 2) count++
        }
    }
    return count;
}

function creerCarte(input, n){
    let carte = initCarte(n,n);
    input.forEach((ligne) => {
       switch (getDirection(ligne[0], ligne[1])){
           case 'h':
               makeHLign(carte, ligne[0], ligne[1]);
               break;
           case 'v':
               makeVLign(carte, ligne[0], ligne[1]);
               break;
           case 'd':
               makeDLign(carte, ligne[0], ligne[1]);
               break;
           default:
               console.log('error');
               break;
       }
    })
    return carte;
}

function Point(x,y){
    this.x = x;
    this.y = y;
}

function getDirection(point1, point2){
    if(point1.y === point2.y) return 'h';
    else if(point1.x === point2.x) return 'v';
    else if(Math.abs(point1.x - point2.x) === Math.abs(point1.y - point2.y)) return 'd';
    else return '?';
}

function makeDLign(map, point1, point2){
    let depart, arrivee;
    if (point1.x < point2.x){
        depart = point1;
        arrivee = point2;
    }
    else{
        depart = point2;
        arrivee = point1;
    }
    if(depart.y > arrivee.y){
        for(let i = 0; i<= (Math.abs(depart.y-arrivee.y)); i++){
            map[depart.x+i][depart.y-i] += 1;
        }
    }
    else{
        for(let i = 0; i<= (Math.abs(depart.y-arrivee.y)); i++){
            map[depart.x+i][depart.y+i] += 1;
        }
    }
}

function makeVLign(map, point1, point2){
    let depart, arrivee;
    if (point1.y < point2.y){
        depart = point1;
        arrivee = point2;
    }
    else{
        depart = point2;
        arrivee = point1;
    }
    for(let i = 0; i<= (arrivee.y-depart.y); i++){
        map[depart.x][depart.y+i] += 1;
    }
}

function makeHLign(map, point1, point2){
    let depart, arrivee;
    if (point1.x < point2.x){
        depart = point1;
        arrivee = point2;
    }
    else{
        depart = point2;
        arrivee = point1;
    }
    for(let i = 0; i<= (arrivee.x-depart.x); i++){
        map[depart.x+i][depart.y] += 1;
    }
}
const lines = getFile('./inputs/jour5.txt').split(/\r\n/);
const x = [];
lines.forEach(line => {
    const p1 = line.split(' -> ')[0].split(',').map(item => parseInt(item,10));
    const p2 = line.split(' -> ')[1].split(',').map(item => parseInt(item,10));
    x.push([new Point(p1[0],p1[1]), new Point(p2[0],p2[1])]);
})
console.log(getNbIntersect(x, 999))
