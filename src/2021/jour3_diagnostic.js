import {getFile} from "../../utilities/utilities.js";

function getLifeSupport(input){
    return getOxygenRating(input) * getCo2Rating(input);
}

function getOxygenRating(input){
    let i =0;
    do{
        let bitCriteria = getGammaArray(input).toString().replaceAll(',', '');
        input = input.filter((item)=>{return item[i] === bitCriteria[i]});
        i++;
    } while(input.length>1);
    return parseInt(input[0],2);
}

function getCo2Rating(input){
    let i =0;
    do{
        let bitCriteria = getEpsilonArray(input).toString().replaceAll(',', '');
        input = input.filter((item)=>{return item[i] === bitCriteria[i]});
        i++;
    } while(input.length>1);
    return parseInt(input[0],2);
}

function powerConsumption(input) {
    const e = getEpsilon(input);
    const g = getGamma(input);
    return e * g;
}

function getGamma(input) {
    const gamma = getGammaArray(input).toString();
    return parseInt(gamma.replaceAll(',', ''), 2);
}

function getEpsilon(input) {
    const epsilon = getEpsilonArray(input).toString();
    return parseInt(epsilon.replaceAll(',', ''), 2);
}

//LeastCommon
function getEpsilonArray(data) {
    const input = constructMax(data);
    const max = [];
    input.forEach((sum, index) => {
        max[index] = sum < data.length / 2 ? 1 : 0;
    })
    return max;
}

//MostCommon
function getGammaArray(data) {
    const input = constructMax(data);
    const max = [];
    input.forEach((sum, index) => {
        max[index] = sum >= (data.length / 2) ? 1 : 0;
    })
    return max;
}

function constructMax(input) {
    const max = [];
    [...input[0]].forEach((i, index) => {
        max[index] = 0;
    });
    input.forEach((item) => {
        for (let i = 0; i < item.length; i++) {
            max[i] += parseInt(item[i], 2);
        }
    })
    return max;
}

const rays = getFile('./inputs/jour3.txt').split(/\n/);
console.log('Life support', getLifeSupport(rays)); //j2

export{powerConsumption, getLifeSupport}
