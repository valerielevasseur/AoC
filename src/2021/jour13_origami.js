import {getFile} from "../../utilities/utilities.js";

function foldY(position, dots) {
    const nbDots = dots.length;
    for (let i = 0; i < nbDots; i++) {
        const dot = dots.shift();
        if (dot[1] > position) {
            dot[1] -= ((dot[1]-position) * 2);
            if (dot[1] !== position && dots.findIndex(
                (current) => current[0] === dot[0] && current[1] === dot[1]) === -1) {
                dots.push(dot);
            }
        }
        else if(dot[1] < position) {
            dots.push(dot)
        }
    }
}

function foldX(position, dots) {
    const nbDots = dots.length;
    for (let i = 0; i < nbDots; i++) {
        const dot = dots.shift();
        if (dot[0] > position) {
            dot[0] -= ((dot[0]-position) * 2);
            if (dot[0] !== position && dots.findIndex(
                (current) => current[0] === dot[0] && current[1] === dot[1]) === -1) {
                dots.push(dot);
            }
        } else if(dot[0] < position) {
            dots.push(dot)
        }
    }
}

function normalize(dots){
    let minX = Number.MAX_VALUE;
    let minY = Number.MAX_VALUE;
    dots.forEach((dot) => {
        if(dot[0] < minX) minX = dot[0];
        if(dot[1] < minY) minY = dot[1];
    })
    dots.forEach((dot) => {
        dot[0] -= minX;
        dot[1] -= minY;
    })
}

function print(dots, n){
    let maxX = 0;
    let maxY = 0;
    dots.forEach((dot) => {
        if(dot[0] > maxX) maxX = dot[0];
        if(dot[1] > maxY) maxY = dot[1];
    })
    let nb = 0;
    for(let j=0; j<=maxY; j++){
        let ligne = "";
        for(let i=0; i<=maxX; i++){
            if(dots.findIndex((dot) => (dot[0] === i && dot[1] === j)) === -1){
                ligne += ' '
            }
            else{
                ligne += '$'
                nb++;
            }
        }
        console.log(ligne)
    }
    return maxX
}

function part1(input) {
    const position = /(?<pos>[0-9]+$)/.exec(input.instructions[0]).groups.pos;
    if (input.instructions[0].includes("y")) {
        foldY(parseInt(position,10), input.dots)
    } else if (input.instructions[0].includes("x")) {
        foldX(parseInt(position,10), input.dots)
    }
    return input.dots.length;
}

function part2(input) {
    input.instructions.forEach((instruction) => {
        const position = /(?<pos>[0-9]+$)/.exec(instruction).groups.pos;
        if (instruction.includes("y")) {
            foldY(parseInt(position,10), input.dots)
        } else if (instruction.includes("x")) {
            foldX(parseInt(position,10), input.dots)
        }
    })
    print(input.dots, 164);
    return input.dots.length;
}

const origami = {};
const input = getFile('./inputs/jour13.txt').split(/\r\n\r\n/);
origami.dots = input.shift().split(/\r\n/).map(item => [parseInt(item.split(',')[0],10),parseInt(item.split(',')[1],10)]);
origami.instructions = input[0].split(/\r\n/);
console.log(part1(origami))
console.log(part2(origami))
