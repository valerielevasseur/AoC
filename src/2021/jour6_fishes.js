import {getFile} from "../../utilities/utilities.js";

function Fish(timer) {
    this.timer = timer;
    this.live = function () {
        if (this.timer === 0) {
            this.timer = 6;
            return this.breed();
        } else {
            this.timer--
            return null;
        }
    }
    this.breed = function () {
        return new Fish(9)
    }
}

function fishLife(fishes, days) {
    for (let i = 1; i <= days; i++) {
        for (let f=0; f<fishes.length; f++) {
            if(fishes[f] === 0){
                fishes[f] = 6;
                fishes.push(9);
            }
            else{
                fishes[f]--;
            }
        }
    }
    return fishes.length;
}

function eternalFishLife(input, days) {
    let number = 0;
    let offsprings = new Array(9).fill(0);
    //Nombre de chaque jour
    for (let fish of input){
        offsprings[fish]++;
    }
    for (let i = 1; i <= days; i++) {
        let next = offsprings.shift(); //live (--)
        offsprings.push(next); //ceux à 0 donnent naissance
        offsprings[6] += next; //retour à 7 pour ceux qui viennent de donner naissance
    }
    for (let fish of offsprings){
        number+= fish;
    }
    return number;
}

const input = getFile('./inputs/jour6.txt').split(',').map(item => parseInt(item,10));;
//console.log(fishLife(input, 80))
console.log(eternalFishLife(input, 256));
