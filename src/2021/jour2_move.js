import {getFile} from "../../utilities/utilities.js";

function Position(horizontal = 0, depth = 0) {
    this.horizontal = horizontal;
    this.depth = 0;
    this.forward = (units) => {
        this.horizontal += units
    };
    this.down = (units) => {
        this.depth += units
    };
    this.up = (units) => {
        this.depth -= units
    };
    this.getMult = () => this.horizontal * this.depth;
}

function PositionAim(horizontal = 0, depth = 0, aim = 0) {
    this.horizontal = horizontal;
    this.depth = 0;
    this.aim = 0;
    this.forward = (units) => {
        this.horizontal += units;
        this.depth += (this.aim * units);
    };
    this.down = (units) => {
        this.aim += units
    };
    this.up = (units) => {
        this.aim -= units
    };
    this.getMult = () => this.horizontal * this.depth;
}

function moveSubmarine(input, aim = true) {
    const position = aim ? new PositionAim() : new Position();
    input.forEach((item) => {
        move(item, position);
    })
    return position.getMult();
}

function move(input, position) {
    const regex = /(?<direction>\w+)\s(?<units>\d+)/;
    const direction = regex.exec(input).groups["direction"];
    const units = parseInt(regex.exec(input).groups["units"], 10);
    switch (direction) {
        case 'forward':
            position.forward(units);
            break;
        case 'down':
            position.down(units);
            break;
        case 'up':
            position.up(units);
            break;
    }
}

const sub = getFile('./inputs/jour2.txt').split(/\n/);
console.log(moveSubmarine(sub, false)); //j1
console.log(moveSubmarine(sub, true)); //j2
