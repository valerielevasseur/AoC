import {getFile} from "../../utilities/utilities.js";

function initPairs(template) {
    const pairs = new Map();
    for (let index = 0; index < template.length - 1; index++) {
        let pair = template.substring(index, index + 2);
        pairs.set(pair, pairs.get(pair) ? pairs.get(pair) + 1 : 1);
    }
    return pairs;
}

function polymerizationMap(rules, pairs, chars = new Map()) {
    let newMap = new Map();
    pairs.forEach((count, pair) => {
        let char = rules.get(pair);
        if (char) {
            chars.set(char, chars.get(char) ? chars.get(char) + count : count);
            let newPairs = [pair[0].concat(char), char.concat(pair[1])]
            newPairs.forEach((newPair) => {
                newMap.set(newPair, newMap.get(newPair) ? newMap.get(newPair) + count : count);
            })
        }
    })
    return newMap;
}

function getMap(string) {
    let chars = new Set(string.split(''));
    let map = new Map();
    chars.forEach((char) => {
        map.set(char, string.match(new RegExp(char, 'g')).length);
    })
    return map;
}

function getMinMax(map) {
    let min = {char: '!', count: Number.MAX_VALUE};
    let max = {char: '!', count: -1};
    map.forEach((count, char) => {
        if (count > max.count) {
            max.char = char;
            max.count = count;
        }
        if (count < min.count) {
            min.char = char;
            min.count = count;
        }
    })
    return max.count - min.count;
}

function polymerization(rules, template, steps = 1) {
    let map = getMap(template)
    let pairs = initPairs(template);
    while (steps > 0) {
        pairs = polymerizationMap(rules, pairs, map)
        steps--
    }
    return getMinMax(map);
}
const input = getFile('./inputs/jour14.txt').split(/\r\n\r\n/);
const template = input.shift();
const rules = new Map();
input[0].split(/\r\n/).forEach(rule => {
    rules.set(rule.split(' -> ')[0], rule.split(' -> ')[1]);
})

console.log(polymerization(rules, template, 10));
console.log(polymerization(rules, template, 40));
