import {getFile} from "../../utilities/utilities.js";

function markBoard(board, draw) {
    board.forEach((ligne, indexL) => {
        ligne.forEach((nombre, indexC) => {
            if (nombre === draw) board[indexL][indexC] = -1
        })
    })
}

function getSomme(board) {
    let somme = 0;
    board.forEach((ligne) => {
        ligne.forEach((nombre) => {
            if (nombre !== -1) somme += nombre;
        })
    })
    return somme;
}

function checkBoard(board) {
    let winner = false;
    const isMarked = (currentValue) => currentValue === -1;
    for (let ligne of board) {
        if (ligne.every(isMarked)) {
            winner = true;
            break;
        }
    }
    let sumCol = 0;
    for (let j = 0; j < board[0].length; j++) {
        sumCol = 0;
        for (let i = 0; i < board[0].length; i++) {
            sumCol += board[i][j]
            if (sumCol === -5) {
                winner = true;
                break;
            }
        }
        if (winner) break;
    }
    return winner;
}

function playFirst(input) {
    let score = 0;
    for (let number of input.draw) {
        let winner = false;
        for (let board of input.boards) {
            markBoard(board, number);
            if (checkBoard(board)) {
                winner = true;
                score = getSomme(board) * number
                break;
            }
        }
        if (winner) break;
    }
    return score;
}

function playLast(input) {
    let score = 0;
    let winners = [];
    let over = false;
    for (let number of input.draw) {
        let index = 0;
        for (let board of input.boards) {
            markBoard(board, number);
            if (checkBoard(board)) {
                if (!winners.includes(index)) winners.push(index);
                if (winners.length === input.boards.length) {
                    score = getSomme(board) * number
                    over = true;
                    break;
                }
            }
            index++;
        }
        if (over) break;
    }
    return score;
}

const squid = {};
const input = getFile('./inputs/jour4.txt').split(/\r\n\r\n/);
squid.draw = input.shift().split(',').map(item => parseInt(item,10));
squid.boards = [];
input.forEach((board) => {
    const lines = board.split(/\r\n/);
    squid.boards.push(board.split(/\r\n/).map(item => item.trim().split(/ {1,2}/).map(item => parseInt(item,10))));
})
console.log(playFirst(squid));
console.log(playLast(squid));

export{playLast, playFirst}
