import {getFile} from "../../utilities/utilities.js";

function Node(id) {
    this.id = id;
    this.adjacents = [];
    this.maxVisites = 1;
    this.visites = 0
}

//Regex
const getNodes = /((?<debut>[a-z]{3,5})|(?<debut_maj>[A-Z]+)|(?<debut_min>[a-z]+))-((?<fin>[a-z]{3,5})|(?<fin_maj>[A-Z]+)|(?<fin_min>[a-z]+))/

function manageNode(input, listeNodes) {
    const res = getNodes.exec(input);
    let info = [res.groups.debut || res.groups.debut_maj || res.groups.debut_min, res.groups.fin || res.groups.fin_maj || res.groups.fin_min]
    info.forEach((newId, index) => {
        //Créer nouvelles nodes au besoin
        let position = listeNodes.findIndex((node) => node.id === newId);
        if (position === -1) { //N'existe pas - ajouter
            listeNodes.push(new Node(newId));
            position = listeNodes.length - 1;
        }
        //Debut
        if (index === 0) {
            const adj = res.groups.fin || res.groups.fin_maj || res.groups.fin_min;
            if (adj && adj !== 'start' && listeNodes[position].id !== 'end') listeNodes[position].adjacents.push(adj);
            if (res.groups.debut_maj) listeNodes[position].maxVisites = Number.MAX_VALUE;
        } else {
            const adj = res.groups.debut || res.groups.debut_maj || res.groups.debut_min;
            if (adj && listeNodes[position].id !== 'end' && adj !== 'start' &&
                listeNodes[position].adjacents.findIndex((node) => node.id === adj) === -1) listeNodes[position].adjacents.push(adj)
            if (res.groups.fin_maj) listeNodes[position].maxVisites = Number.MAX_VALUE;
        }
    });
}

function visitAdjacents(node, listeNodes, total, firstCave) {
    for (let i=0; i<node.adjacents.length; i++) {
        let adjId = node.adjacents[i]
        // console.log(node.id, '-', adjId)
        const adjNode = listeNodes.find((node) => node.id === adjId);
        if (adjNode.id === 'end') {
            total++
        }
        else if (adjNode.visites < adjNode.maxVisites || firstCave && adjNode.id === firstCave && adjNode.visites ===1) {
            adjNode.visites++;
            total = visitAdjacents(adjNode, listeNodes, total, firstCave);
        }
    }
    node.visites--;
    return total;
}

function part1(input) {
    let listeNodes = [];
    input.forEach((info) => {
        manageNode(info, listeNodes);
    })
    //Carte créée
    // console.log(listeNodes)

    //Part1
    let nbCheminsBase = 0
    nbCheminsBase += visitAdjacents(listeNodes.find((node) => node.id === 'start'), listeNodes, 0, null);

    //Part2
    let nbChemins = nbCheminsBase;
    for(let el of listeNodes.filter((el) => /^[a-z]{1,2}$/.test(el.id))){
        let chemins = 0;
        chemins += visitAdjacents(listeNodes.find((node) => node.id === 'start'), listeNodes, 0, el.id);
        nbChemins += chemins > 0 ? chemins-nbCheminsBase : 0;
    }
    return nbChemins;
}
const real= getFile('./inputs/jour12.txt').split(/\r\n/);
console.log(part1(real));
