import {getFile} from "../../utilities/utilities.js";

function Signal(pattern, numbers) {
    this.pattern = pattern;
    this.numbers = numbers;
}

const letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']

//[a,b,c,d,e,f,g] où chaque lettre est true ou false
function getDisplay(letters) {
    return [letters.includes('a'), letters.includes('b'), letters.includes('c'),
        letters.includes('d'), letters.includes('e'),
        letters.includes('f'), letters.includes('g')];
}

function sumArray(input) {
    const array = input.slice();
    const reducer = (total, current) => total + current;
    return (array.reduce(reducer));
}

function displayToString(array) {
    let response = '';
    for (let i = 0; i < 7; i++) {
        if (array[i]) response += letters[i];
    }
    return response;
}

function soustraction(input1, input2) {
    const array1 = input1.slice();
    const array2 = input2.slice();
    for (let i = 0; i < 7; i++) {
        if (array1[i]) {
            array1[i] = array2[i] !== true;
        }
    }
    return array1;
}

function addition(input1, input2) {
    const array1 = input1.slice();
    const array2 = input2.slice();
    for (let i = 0; i < 7; i++) {
        if (array1[i] === true || array2[i] === true) {
            array1[i] = true;
        }
    }
    return array1;
}

function isSame(array1, array2) {
    for (let i = 0; i < 7; i++) {
        if (array1[i] !== array2[i]) {
            return false;
        }
    }
    return true;
}

function getFromSum(array, somme) {
    const index = array.findIndex(element => sumArray(element) === somme);
    return {element: array[index], array: array.splice(index, 1)}
}

function nbGetDiff(display1, display2) {
    let diff = 0;
    for (let i = 0; i < 7; i++) {
        if (display1[i] !== display2[i]) diff++;
    }
    return diff;
}

function getFromDiff(array, element2, diff) {
    const index = array.findIndex(element => nbGetDiff(element, element2) === diff);
    return {element: array[index], array: array.splice(index, 1)}
}

function getMixedDisplay(input) {
    const currentDisplay = input.split(' ').map(x => getDisplay(x));
    let orderedDisplay = new Array(9);
    const traduction = {};
    /*
     * 2/7: 1
     * 3/7: 7
     * 4/7: 4
     * 5/7: 2, 3, 5
     * 6/7: 0, 6, 9
     * 7/7: 8
     */
    orderedDisplay[1] = getFromSum(currentDisplay, 2).element;
    orderedDisplay[4] = getFromSum(currentDisplay, 4).element;
    orderedDisplay[7] = getFromSum(currentDisplay, 3).element
    orderedDisplay[8] = getFromSum(currentDisplay, 7).element;
    traduction.a = soustraction(orderedDisplay[7], orderedDisplay[1]);
    const nineMinusg = addition(orderedDisplay[4], traduction.a);
    orderedDisplay[9] = getFromDiff(currentDisplay, nineMinusg, 1).element;
    //0,6,2,3,5
    for (let i = 0; i < currentDisplay.length; i++) {
        if (sumArray(currentDisplay[i]) === 6) {
            const sum = sumArray(addition(currentDisplay[i], orderedDisplay[1]))
            if (sum === 7) {
                orderedDisplay[6] = currentDisplay[i];
            } else if (sum === 6) {
                orderedDisplay[0] = currentDisplay[i];
            }
        } else if (sumArray(soustraction(currentDisplay[i], orderedDisplay[1])) === 3) {
            orderedDisplay[3] = currentDisplay[i];
        } else if (sumArray(addition(currentDisplay[i], orderedDisplay[4])) === 7) {
            orderedDisplay[2] = currentDisplay[i];
        } else {
            orderedDisplay[5] = currentDisplay[i];
        }
    }
    return orderedDisplay;
}

function traduction(input) {
    let number = "";
    const orderedDisplay = getMixedDisplay(input.pattern);
    for (let element of input.numbers.split(' ')) {
        const arrayEl = getDisplay(element);
        const index = orderedDisplay.findIndex(candidate => isSame(candidate, arrayEl));
        number += (index.toString(10));
    }
    return number;
}



const correctDisplay = {
    //[a,b,c,d,e,f,g]
    0: [1, 1, 1, 0, 1, 1, 1],
    1: [0, 0, 1, 0, 0, 1, 0],
    2: [1, 0, 1, 1, 1, 0, 1],
    3: [1, 0, 1, 1, 0, 1, 1],
    4: [0, 1, 1, 1, 0, 1, 0],
    5: [1, 1, 0, 1, 0, 1, 1],
    6: [1, 1, 0, 1, 1, 1, 1],
    7: [1, 0, 1, 0, 0, 1, 0],
    8: [1, 1, 1, 1, 1, 1, 1],
    9: [1, 1, 1, 1, 0, 1, 1],
}


function part1(input){
    let response = "";
    input.forEach((signal) =>{
        response += traduction(signal);
    })
    //1, 4, 7, or 8
    return Array.from(response).filter(number => (number === '1' || number === '4'|| number === '7' || number === '8')).length;
}

function part2(input){
    let response = 0;
    input.forEach((signal) =>{
        response += parseInt(traduction(signal));
    })
    return response;
}

const input = getFile('./inputs/jour8.txt').split(/\n/);
const list = [];
input.forEach(entry => {
    list.push(new Signal(entry.split(' | ')[0], entry.split(' | ')[1].trim()))
})

console.log(part1(list))
console.log(part2(list))
