import * as assert from "assert";
import {getNbVisible, Tree, checkLeft, checkRight, checkUp, checkDown} from "../src/2022/jour8_tree_house.js";

const treeX = new Tree(0, 1, 3)
const visibleLeft = [[new Tree(0, 0, 1), treeX, new Tree(0, 2, 4)]];
const visibleRight = [[new Tree(0, 0, 4), treeX, new Tree(0, 2, 1)]];
const visibleBothX = [[new Tree(0, 0, 1), treeX, new Tree(0, 2, 1)]];

const treeY = new Tree(1, 0, 3)
const visibleUp = [[new Tree(0, 0, 1)],[treeY],[new Tree(2, 0, 4)]];
const visibleDown = [[new Tree(0, 0, 4)],[treeY],[new Tree(2, 0, 1)]];
const visibleBothY = [[new Tree(0, 0, 1)],[treeY],[new Tree(2, 0, 1)]];

const treeBx =  new Tree(0, 2, 3);
const treeBy =  new Tree(2, 0, 3);
const treeBy2 =  new Tree(1, 0, 3);

const visibleBLeft = [[new Tree(0, 0, 1), new Tree(0, 1, 2),treeBx, new Tree(0, 3, 4)]]; //1-2-Tree(3)-4
const visibleBRight = [[new Tree(0, 0, 4), treeX, new Tree(0, 2, 2), new Tree(0, 3, 1)]]; //4-3-2-1
const visibleBUp = [[new Tree(0, 0, 1)],[new Tree(1, 0, 1)], [treeBy],[new Tree(3, 0, 4)]]; //1-2-Tree(3)-4
const visibleBDown = [[new Tree(0, 0, 4)],[treeBy2],[new Tree(2, 0, 2)], [new Tree(3, 0, 1)]]; //4-3-2-1

describe('Jour 8', function () {
    describe('CheckLeft', function () {
        it('should be visible from left', function () {
            assert.equal(checkLeft(treeX, visibleLeft), true);
        });
        it('should be visible from left', function () {
            assert.equal(checkLeft(treeX, visibleBothX), true);
        });
        it('should not be visible from left', function () {
            assert.equal(checkLeft(treeX, visibleRight), false);
        });
    });
    describe('CheckRight', function () {
        it('should be visible from right', function () {
            assert.equal(checkRight(treeX, visibleRight), true);
        });
        it('should be visible from right', function () {
            assert.equal(checkRight(treeX, visibleBothX), true);
        });
        it('should not be visible from right', function () {
            assert.equal(checkRight(treeX, visibleLeft), false);
        });
    });
    describe('CheckUp', function () {
        it('should be visible from top', function () {
            assert.equal(checkUp(treeY, visibleUp), true);
        });
        it('should be visible from top', function () {
            assert.equal(checkUp(treeY, visibleBothY), true);
        });
        it('should not be visible from top', function () {
            assert.equal(checkUp(treeY, visibleDown), false);
        });
    });
    describe('CheckDown', function () {
        it('should be visible from bottom', function () {
            assert.equal(checkDown(treeY, visibleDown), true);
        });
        it('should be visible from bottom', function () {
            assert.equal(checkDown(treeY, visibleBothY), true);
        });
        it('should not be visible from bottom', function () {
            assert.equal(checkDown(treeY, visibleUp), false);
        });
    });
    describe('Blocked', function () {
        it('should not be visible from bottomExterior', function () {
            assert.equal(checkDown(treeBy2, visibleBDown), false);
        });
        it('should not be visible from topExterior', function () {
            assert.equal(checkUp(treeBy, visibleBUp), false);
        });
        it('should not be visible from leftExterior', function () {
            assert.equal(checkLeft(treeBx, visibleBLeft), false);
        });
        it('should not be visible from rightExterior', function () {
            assert.equal(checkRight(treeX, visibleBRight), false);
        });
    });
});
